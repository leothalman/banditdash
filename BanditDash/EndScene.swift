//
//  EndScene.swift
//  BanditDash
//
//  Created by Leo Thalman on 4/2/18.
//  Copyright © 2018 Leo Thalman. All rights reserved.
//

import SpriteKit

class EndScene: SKScene {
    
    let title = SKLabelNode(fontNamed: "Chalkduster")
    let play = SKLabelNode(fontNamed: "Chalkduster")
    let home = SKLabelNode(fontNamed: "Chalkduster")
    
    
    override func didMove(to view: SKView) {
        title.text = "Game Over"
        title.fontSize = 45
        title.fontColor = SKColor.black
        title.position = CGPoint(x: frame.midX, y: 300)
        self.addChild(title)
        
        play.text = "Run Again"
        play.fontSize = 35
        play.fontColor = SKColor.black
        play.position = CGPoint(x: frame.midX + (frame.midX/2) , y: 20)
        
        home.text = "Back To Home"
        home.fontSize = 35
        home.fontColor = SKColor.black
        home.position = CGPoint(x: 160, y: 20)
        
        self.addChild(play)
        self.addChild(home)
        
        let r = CGFloat(0.97)
        let g = CGFloat(0.80)
        let b = CGFloat(0.68)
        var a = CGFloat(1.0)
        self.backgroundColor = UIColor(red: r, green: g, blue: b, alpha: a)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
            let pos = touch.location(in: self)
            let node = self.atPoint(pos)
            
            if node == play {
                
                    let transition:SKTransition = SKTransition.fade(withDuration: 1)
                    //guard let scene = GameScene(fileNamed: "GameScene") else { return }
                    let scene:SKScene = GameScene(size: self.size)
                    self.view?.presentScene(scene, transition: transition)
            }
                
                if node == home {
                    
                        let transition:SKTransition = SKTransition.fade(withDuration: 1)
                        let scene:SKScene = HomeScene(size: self.size)
                        self.view?.presentScene(scene, transition: transition)
                    }
            }
        }
    }


