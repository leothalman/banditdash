# Welcome
Welcome to my Individual Project Repository. This Repo houses the code for my individual project Bandit Dash,
an iOS platformer game that is coded in swift and uses the Spritekit library.

# Bandit Dash

Bandit Dash is a 2D platformer game with some roguelike elements.

### Vision Statement
> For players who want a mobile 2D platformer with the randomness of roguelike games.
Bandit Dash is an iOS based platforming game that randomly creates the level as the player
progresses through so that no two runs are alike. Score in Bandit Dash is based off how the
player gets in their run. Unlike current mobile platformers and endless runners, Bandit Dash
combines the two offering players a game with more control over your character for a
more interactive experience that can be as short or long as the player wants.

### Contributing
* To submit work for Bandit Dash use the forking workflow detailed [here](https://www.atlassian.com/git/tutorials/comparing-workflows/forking-workflow).
* The coding conventions for this project are the standard swift API guidelines detailed [here](https://swift.org/documentation/api-design-guidelines/).
* If you wish to contribute to the project be sure to process detailed in the tutorial and make sure to add your name to the [contributors file](contributors.md) when you first submit work.
* Any questions can be directed towards lthalman15@mail.wou.edu.