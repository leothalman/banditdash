Milestone 6

_________________________________________________________________________________
--Needs and Features--

Users
	--Players
		--Control a character
		--Name a character
		--Customize character
		--Progress through world with character
		--View leaderboards
		--Buy items for character (with in-game coin)

	--Generic
		--World is randomized each run
		--Seed?

_________________________________________________________________________________
--Requirements--

--Plays well on an iPhone and iPad
--Leaderboard to keep track of scores globally
_________________________________________________________________________________
--Vision Statement--

For players who want a mobile 2D platformer with the randomness of roguelike games. 
Bandit Dash is an iOS based platforming game that randomly creates the level as the 
player progresses through so that no two runs are alike. Score in Bandit Dash is based 
off how the player gets in their run. Unlike current mobile platformers and endless runners, 
Bandit Dash combines the two offering players a game with more control over your 
character for a more interactive experience that can be as short or long as the player wants.

_________________________________________________________________________________
--Identification of Risks--
-- Randomized level generation
-- Possibly too similar to other platformers
-- Leaderboards

