//
//  HomeScene.swift
//  BanditDash
//
//  Created by Leo Thalman on 4/2/18.
//  Copyright © 2018 Leo Thalman. All rights reserved.
//

import SpriteKit

class HomeScene: SKScene {
    
    let title = SKLabelNode(fontNamed: "Chalkduster")
    let play = SKLabelNode(fontNamed: "Chalkduster")
    let tutorial = SKLabelNode(fontNamed: "Chalkduster")

    
    override func didMove(to view: SKView) {
        title.text = "Welcome To Bandit Dash!"
        title.fontSize = 45
        title.fontColor = SKColor.black
        title.position = CGPoint(x: frame.midX, y: 300)
        self.addChild(title)
        
        play.text = "Start Run"
        play.fontSize = 35
        play.fontColor = SKColor.black
        play.position = CGPoint(x: 120, y: 20)
        self.addChild(play)
        
        tutorial.text = "Tutorial"
        tutorial.fontSize = 35
        tutorial.fontColor = SKColor.black
        tutorial.position = CGPoint(x: frame.maxX - 120, y: 20)
        self.addChild(tutorial)
        
        let r = CGFloat(0.97)
        let g = CGFloat(0.80)
        let b = CGFloat(0.68)
        let a = CGFloat(1.0)
        self.backgroundColor = UIColor(red: r, green: g, blue: b, alpha: a)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
            let pos = touch.location(in: self)
            let node = self.atPoint(pos)
            
            if node == play {
                    let transition:SKTransition = SKTransition.fade(withDuration: 1)
                    //guard let scene = GameScene(fileNamed: "GameScene") else { return }
                    let scene:SKScene = GameScene(size: self.size)
                    self.view?.presentScene(scene, transition: transition)
                }
            if node == tutorial{
                let transition:SKTransition = SKTransition.fade(withDuration: 1)
                //guard let scene = GameScene(fileNamed: "GameScene") else { return }
                let scene:SKScene = TutorialScene(size: self.size)
                self.view?.presentScene(scene, transition: transition)
            }
            }
        }
    }

