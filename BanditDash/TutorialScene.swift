import SpriteKit

class TutorialScene: SKScene {
    
    struct PhysicsCategory {
        static let none      : UInt32 = 0
        static let all       : UInt32 = UInt32.max
        static let floor     : UInt32 = 0b1       // 1
        static let hazard    : UInt32 = 0b10      // 2
        static let pBit      : UInt32 = 0b100
        static let enemy     : UInt32 = 0b1000
        static let endFlag   : UInt32 = 0b10000
    }
    
    var arraySprites :[SKSpriteNode] = [SKSpriteNode]()
    var moveNum: Int = 0
    var enemyMoveDistance: Int = 100
    var movingLeft: Bool = false
    var movingRight: Bool = false
    var jumping: Bool = false
    var dashing: Bool = false
    //true if facing right, false if facing left
    var facing: Bool = true
    var canDash: Bool = true
    var cam: SKCameraNode?
    var isAnimated: Bool = false
    
    var levelBackground:SKTileMapNode!
    var hazards:SKTileMapNode!
    
    let player = SKSpriteNode(imageNamed: "player-idle")
    var playerWalking: [SKTexture] = []
    let endFlag = SKSpriteNode (imageNamed: "goal")
    
    
    let lButton = SKSpriteNode(imageNamed: "left")
    let rButton = SKSpriteNode(imageNamed: "right")
    let dButton = SKSpriteNode(imageNamed: "dash")
    let jButton = SKSpriteNode(imageNamed: "jump")
    
    let score = SKLabelNode(fontNamed: "Chalkduster")
    
    override func didMove(to view: SKView) {
        //create the level
        createLevel()
        
        physicsWorld.contactDelegate = self
        
        //player
        player.position = CGPoint(x: 0, y: 0)
        player.size = CGSize(width: 32, height: 32)
        player.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: 31,
                                                               height: 31))
        player.physicsBody?.affectedByGravity = true
        player.physicsBody?.allowsRotation = false
        player.physicsBody?.restitution = 0.0
        player.physicsBody?.categoryBitMask = PhysicsCategory.pBit
        player.physicsBody?.collisionBitMask = PhysicsCategory.floor
        player.physicsBody?.usesPreciseCollisionDetection = true
        
        buildPlayerAnimation()
        
        addChild(player)
        
        
        //camera
        cam = SKCameraNode()
        self.camera = cam
        camera!.position.y = 100
        self.addChild(cam!)
        
        //end test
        score.text = "Score Displayed Here"
        score.fontSize = 25
        score.fontColor = SKColor.black
        score.position = CGPoint(x: 0, y: 140)
        self.camera?.addChild(score)
        
        
        //left button properties
        lButton.position = CGPoint(x: -250 , y: -140 )
        lButton.name = "leftButton"
        lButton.size = CGSize(width: lButton.size.width, height: lButton.size.height)
        self.camera?.addChild(lButton)
        
        //right button properties
        rButton.position = CGPoint(x: -120 , y: -140 )
        rButton.name = "rightButton"
        rButton.size = CGSize(width: rButton.size.width, height: rButton.size.height)
        self.camera?.addChild(rButton)
        
        //dash button properties
        dButton.position = CGPoint(x:  120 , y: -140 )
        dButton.name = "dash"
        dButton.size = CGSize(width: dButton.size.width, height: dButton.size.height)
        self.camera?.addChild(dButton)
        
        //jump button properties
        jButton.position = CGPoint(x: 250 , y: -140 )
        jButton.name = "jump"
        jButton.size = CGSize(width: jButton.size.width, height: jButton.size.height)
        self.camera?.addChild(jButton)
        
        setLabels()
    }
    
    func setLabels()
    {
        let leftLabel = SKLabelNode(fontNamed: "Chalkduster")
        let rightLabel = SKLabelNode(fontNamed: "Chalkduster")
        let dashLabel = SKLabelNode(fontNamed: "Chalkduster")
        let jumpLabel = SKLabelNode(fontNamed: "Chalkduster")
        
        let rtnLabel = SKLabelNode(fontNamed: "Chalkduster")
        let spikeLabel = SKLabelNode(fontNamed: "Chalkduster")
        let enemyLabel = SKLabelNode(fontNamed: "Chalkduster")
        let enemyLabel2 = SKLabelNode(fontNamed: "Chalkduster")
        let endLabel = SKLabelNode(fontNamed: "Chalkduster")
        let endLabel2 = SKLabelNode(fontNamed: "Chalkduster")
        
        leftLabel.text = "Press to go left"
        leftLabel.fontSize = 15
        leftLabel.fontColor = SKColor.black
        leftLabel.position = CGPoint(x: -250 , y: -100 )
        self.camera?.addChild(leftLabel)
        
        rightLabel.text = "Press to go right"
        rightLabel.fontSize = 15
        rightLabel.fontColor = SKColor.black
        rightLabel.position = CGPoint(x: -100 , y: -100 )
        self.camera?.addChild(rightLabel)
        
        dashLabel.text = "Press to dash"
        dashLabel.fontSize = 15
        dashLabel.fontColor = SKColor.black
        dashLabel.position = CGPoint(x: 120 , y: -100 )
        self.camera?.addChild(dashLabel)
        
        jumpLabel.text = "Press to jump"
        jumpLabel.fontSize = 15
        jumpLabel.fontColor = SKColor.black
        jumpLabel.position = CGPoint(x: 250 , y: -100 )
        self.camera?.addChild(jumpLabel)
        
        rtnLabel.name = "rtnLabel"
        rtnLabel.text = "Back to Home"
        rtnLabel.fontSize = 15
        rtnLabel.fontColor = SKColor.black
        rtnLabel.position = CGPoint(x: 250 , y: 140 )
        self.camera?.addChild(rtnLabel)
        
        spikeLabel.text = "Hitting spikes will end the run"
        spikeLabel.fontSize = 15
        spikeLabel.fontColor = SKColor.black
        spikeLabel.position = CGPoint(x: 15 * 32 , y: 40 )
        self.addChild(spikeLabel)
        
        enemyLabel.text = "Dash into enemies to defeat them"
        enemyLabel.fontSize = 15
        enemyLabel.fontColor = SKColor.black
        enemyLabel.position = CGPoint(x: 28 * 32 , y: 60 )
        self.addChild(enemyLabel)
        
        enemyLabel2.text = "Running into or jumping on them will end the run"
        enemyLabel2.fontSize = 15
        enemyLabel2.fontColor = SKColor.black
        enemyLabel2.position = CGPoint(x: 28 * 32 , y: 40 )
        self.addChild(enemyLabel2)
        
        endLabel.text = "Potions are at the end of every level"
        endLabel.fontSize = 15
        endLabel.fontColor = SKColor.black
        endLabel.position = CGPoint(x: 39 * 32 , y: 22 )
        self.addChild(endLabel)
        
        endLabel2.text = "Grab them to go to the next level"
        endLabel2.fontSize = 15
        endLabel2.fontColor = SKColor.black
        endLabel2.position = CGPoint(x: 39 * 32 , y: 10 )
        self.addChild(endLabel2)
        
        
        
    }
    
    func buildPlayerAnimation() {
        let playerAnimatedAtlas = SKTextureAtlas(named: "playerMoving")
        var walkFrames: [SKTexture] = []
        
        let numImages = playerAnimatedAtlas.textureNames.count
        for i in 2...numImages {
            let playerTextureName = "player-move\(i)"
            walkFrames.append(playerAnimatedAtlas.textureNamed(playerTextureName))
        }
        playerWalking = walkFrames
    }
    
    func playerAnimate(){
        player.run(SKAction.repeatForever(
            SKAction.animate(with: playerWalking, timePerFrame: 0.1,
                             resize: false, restore: true)),
                   withKey: "playerIsWalking")
    }
    
    func didBegin(_ contact: SKPhysicsContact) {
        var firstBody: SKPhysicsBody
        var secondBody: SKPhysicsBody
        if contact.bodyA.categoryBitMask < contact.bodyB.categoryBitMask {
            firstBody = contact.bodyA
            secondBody = contact.bodyB
        }
        else {
            firstBody = contact.bodyB
            secondBody = contact.bodyA
        }
        
        if (firstBody.categoryBitMask == PhysicsCategory.floor) &&
            (secondBody.categoryBitMask == PhysicsCategory.pBit){
            let test = CGFloat(0.01)
            let test2 = CGFloat(-0.1)
            if(jumping && (player.physicsBody?.velocity.dy)! < test && (player.physicsBody?.velocity.dy)! > test2)
            {
                jumping = false
                self.player.texture = SKTexture(imageNamed: "player-idle")
                if (movingRight || movingLeft){
                    playerAnimate()
                    if(!isAnimated){
                        isAnimated = true
                    }
                }
            }
            
        }
        if (firstBody.categoryBitMask == PhysicsCategory.hazard) &&
            (secondBody.categoryBitMask == PhysicsCategory.pBit) {
            let transition:SKTransition = SKTransition.fade(withDuration: 1)
            let scene:SKScene = TutorialScene(size: self.size)
            self.view?.presentScene(scene, transition: transition)
        }
        if (firstBody.categoryBitMask == PhysicsCategory.pBit) &&
            (secondBody.categoryBitMask == PhysicsCategory.endFlag) {
            let transition:SKTransition = SKTransition.fade(withDuration: 1)
            let scene:TutorialScene = TutorialScene(size: self.size)
            self.view?.presentScene(scene, transition: transition)
        }
        if (firstBody.categoryBitMask == PhysicsCategory.pBit) &&
            (secondBody.categoryBitMask == PhysicsCategory.enemy) {
            if (dashing){
                secondBody.node?.removeFromParent()
            }
            else{
                let transition:SKTransition = SKTransition.fade(withDuration: 1)
                let scene:SKScene = TutorialScene(size: self.size)
                self.view?.presentScene(scene, transition: transition)
            }
        }
        
    }
    
    func createLevel()
    {
        let r = CGFloat(0.97)
        let g = CGFloat(0.80)
        let b = CGFloat(0.68)
        let a = CGFloat(1.0)
        self.backgroundColor = UIColor(red: r, green: g, blue: b, alpha: a)
        
        //distance between objects
        let objDistance = 10
        
        var objNumber = 0
        //Tile columns
        let col = 50
        //Tile rows
        let row = 10
        //tile sizes
        let xSize = 32
        let ySize = 32
        
        guard let tileSet = SKTileSet(named: "BackgroundTiles") else
        {
            fatalError("Background Tiles not found")
        }
        let levelBackground = SKTileMapNode(tileSet: tileSet, columns: col, rows: row, tileSize: CGSize(width: xSize, height: ySize))
        let hazards = SKTileMapNode(tileSet: tileSet, columns: col, rows: row, tileSize: CGSize(width: xSize, height: ySize))
        levelBackground.position = CGPoint(x: ((col-10)/2) * xSize, y: 0)
        levelBackground.setScale(1)
        hazards.position = CGPoint(x: ((col-10)/2) * xSize, y: 0)
        hazards.setScale(1)
        self.addChild(hazards)
        self.addChild(levelBackground)
        self.hazards = hazards
        self.levelBackground = levelBackground
        
        guard let tile = levelBackground.tileSet.tileGroups.first(where: {$0.name == "floor"})
            else {fatalError("Floor Tile not found")}
        guard let spike = hazards.tileSet.tileGroups.first(where: {$0.name == "spikes"})
            else {fatalError("Spikes Tile not foud")}
        
        for r in 0...(row-1) {
            levelBackground.setTileGroup(tile, forColumn:0, row: r)
            levelBackground.setTileGroup(tile, forColumn:col-1, row: r)
        }
        var enemy = SKSpriteNode (imageNamed: "enemy1")
        for column in 0...(col-1) {
            levelBackground.setTileGroup(tile, forColumn: column, row: 0)
            levelBackground.setTileGroup(tile, forColumn: column, row: (row-1))
        }
        hazards.setTileGroup(spike, forColumn:20, row: 1)
        hazards.setTileGroup(spike, forColumn:21, row: 1)

        levelBackground.setTileGroup(tile, forColumn: 10, row: 1)
        levelBackground.setTileGroup(tile, forColumn: 10, row: 2)

        levelBackground.setTileGroup(tile, forColumn: 30, row: 2)
        levelBackground.setTileGroup(tile, forColumn: 31, row: 2)
        levelBackground.setTileGroup(tile, forColumn: 32, row: 2)

        enemy = SKSpriteNode (imageNamed: "enemy1")
        enemy.position = CGPoint(x: 29 * 32 , y: 32)
        enemy.size = CGSize(width: 32, height: 32)
        enemy.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: 32,
                                                              height: 32))
        enemy.physicsBody?.affectedByGravity = true
        enemy.physicsBody?.allowsRotation = false
        enemy.physicsBody?.restitution = 0.0
        enemy.physicsBody?.categoryBitMask = PhysicsCategory.enemy
        enemy.physicsBody?.collisionBitMask = PhysicsCategory.floor + PhysicsCategory.hazard
        enemy.physicsBody?.usesPreciseCollisionDetection = true
        enemy.physicsBody?.contactTestBitMask = PhysicsCategory.pBit
        self.addChild(enemy)
        
        endFlag.size = CGSize(width: 32, height: 32)
        endFlag.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: endFlag.size.width,
                                                                height: endFlag.size.height))
        endFlag.position = CGPoint(x: 40 * 32 , y: 32)
        endFlag.physicsBody?.categoryBitMask = PhysicsCategory.endFlag
        endFlag.physicsBody?.contactTestBitMask = PhysicsCategory.pBit
        endFlag.physicsBody?.usesPreciseCollisionDetection = true
        self.addChild(endFlag)
        
        giveFloorPhysicsBody(map: levelBackground)
        giveSpikePhysicsBody(map: hazards)
    }
    
    
    func giveFloorPhysicsBody(map: SKTileMapNode)
    {
        let tileMap = map
        
        let tileSize = tileMap.tileSize
        let halfWidth = CGFloat(tileMap.numberOfColumns) / 2.0 * tileSize.width
        let halfHeight = CGFloat(tileMap.numberOfRows) / 2.0 * tileSize.height
        
        for col in 0..<tileMap.numberOfColumns {
            
            for row in 0..<tileMap.numberOfRows {
                
                if let tileDefinition = tileMap.tileDefinition(atColumn: col, row: row)
                    
                {
                    
                    //let isEdgeTile = tileDefinition.userData?["AddBody"] as? Int  //uncomment this if needed, see article notes
                    //if (isEdgeTile != 0) {
                    let tileArray = tileDefinition.textures
                    let tileTexture = tileArray[0]
                    let x = CGFloat(col) * tileSize.width - halfWidth + (tileSize.width/2)
                    let y = CGFloat(row) * tileSize.height - halfHeight + (tileSize.height/2)
                    _ = CGRect(x: 0, y: 0, width: tileSize.width, height: tileSize.height)
                    let tileNode = SKNode()
                    
                    tileNode.position = CGPoint(x: x, y: y)
                    tileNode.physicsBody = SKPhysicsBody(texture: tileTexture, size: CGSize(width: 32, height: 32))
                    tileNode.physicsBody?.linearDamping = 0.0
                    tileNode.physicsBody?.affectedByGravity = false
                    tileNode.physicsBody?.allowsRotation = false
                    tileNode.physicsBody?.restitution = 0.0
                    tileNode.physicsBody?.isDynamic = false
                    
                    tileNode.physicsBody?.categoryBitMask = PhysicsCategory.floor
                    tileNode.physicsBody?.collisionBitMask = PhysicsCategory.pBit
                    tileNode.physicsBody?.contactTestBitMask = PhysicsCategory.pBit
                    
                    
                    //tileNode.physicsBody?.categoryBitMask = BodyType.item.rawValue
                    
                    tileMap.addChild(tileNode)
                    //}
                }
            }
        }
    }
    
    func giveSpikePhysicsBody(map: SKTileMapNode)
    {
        let tileMap = map
        
        let tileSize = tileMap.tileSize
        let halfWidth = CGFloat(tileMap.numberOfColumns) / 2.0 * tileSize.width
        let halfHeight = CGFloat(tileMap.numberOfRows) / 2.0 * tileSize.height
        
        for col in 0..<tileMap.numberOfColumns {
            
            for row in 0..<tileMap.numberOfRows {
                
                if let tileDefinition = tileMap.tileDefinition(atColumn: col, row: row)
                    
                {
                    
                    //let isEdgeTile = tileDefinition.userData?["AddBody"] as? Int  //uncomment this if needed, see article notes
                    //if (isEdgeTile != 0) {
                    let tileArray = tileDefinition.textures
                    let tileTexture = tileArray[0]
                    let x = CGFloat(col) * tileSize.width - halfWidth + (tileSize.width/2)
                    let y = CGFloat(row) * tileSize.height - halfHeight + (tileSize.height/2)
                    _ = CGRect(x: 0, y: 0, width: tileSize.width, height: tileSize.height)
                    let tileNode = SKNode()
                    
                    tileNode.position = CGPoint(x: x, y: y)
                    tileNode.physicsBody = SKPhysicsBody(texture: tileTexture, size: CGSize(width: 30 , height: 30))
                    tileNode.physicsBody?.linearDamping = 0.0
                    tileNode.physicsBody?.affectedByGravity = false
                    tileNode.physicsBody?.allowsRotation = false
                    tileNode.physicsBody?.restitution = 0.0
                    tileNode.physicsBody?.isDynamic = false
                    
                    tileNode.physicsBody?.categoryBitMask = PhysicsCategory.hazard
                    tileNode.physicsBody?.collisionBitMask = PhysicsCategory.none
                    tileNode.physicsBody?.contactTestBitMask = PhysicsCategory.pBit
                    
                    
                    //tileNode.physicsBody?.categoryBitMask = BodyType.item.rawValue
                    
                    tileMap.addChild(tileNode)
                    //}
                }
            }
        }
    }
    
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch: AnyObject in touches {
            let location = touch.location(in:self)
            let node = self.atPoint(location)
            
            if (node.name == "leftButton") {
                movingLeft = true
            }
            if (node.name == "rightButton"){
                movingRight = true
            }
            if (node.name == "rtnLabel"){
                let transition:SKTransition = SKTransition.fade(withDuration: 1)
                let scene:SKScene = HomeScene(size: self.size)
                self.view?.presentScene(scene, transition: transition)
            }
            
            if (node.name == "dash" && canDash) {
                var dashDistance = CGFloat(-100.0)
                if(facing){
                    dashDistance = CGFloat(100.0)
                }
                self.dashing = true
                self.canDash = false
                dButton.texture = SKTexture(imageNamed: "cooldown")
                let wait = SKAction.wait(forDuration:1.2)
                let unflagCanDash = SKAction.run {
                    self.canDash = true
                    self.dButton.texture = SKTexture(imageNamed: "dash")
                }
                let animateDash = SKAction.run{
                    if(self.movingLeft || self.movingRight){
                        self.player.removeAction(forKey: "playerIsWalking")
                    }
                    self.player.texture = SKTexture(imageNamed: "player-move1")
                }
                let dash = SKAction.moveBy(x:dashDistance, y:0.01, duration: 0.1)
                let unflagDashing = SKAction.run {
                    self.dashing = false
                    if(!self.jumping){
                        if(self.movingLeft || self.movingRight){
                            self.playerAnimate()
                        }
                        self.player.texture = SKTexture(imageNamed: "player-idle")
                    }
                }
                let dashSeq = SKAction.sequence([animateDash, dash, unflagDashing, wait, unflagCanDash])
                player.run(dashSeq)
            }
            
            if (node.name == "jump")
            {
                if(!jumping){
                    jumping = true
                    if (movingRight || movingLeft){
                        self.player.removeAction(forKey: "playerIsWalking")
                    }
                    self.player.texture = SKTexture(imageNamed: "player-jump")
                    
                    self.player.physicsBody!.applyImpulse(CGVector(dx:0 , dy: 25))
                }
                
            }
            
        }
    }
    
    //check which button is pressed and move the player in that direction
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?){
        for touch: AnyObject in touches {
            let location = touch.location(in:self)
            let node = self.atPoint(location)
            
            if (node.name == "leftButton") {
                movingLeft = true
            }
            else {
                movingLeft = false
            }
            if (node.name == "rightButton"){
                movingRight = true
            }
            else {
                movingRight = false
            }
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch: AnyObject in touches {
            let location = touch.location(in:self)
            let node = self.atPoint(location)
            
            if (node.name == "leftButton") {
                movingLeft = false
            }
            if (node.name == "rightButton"){
                movingRight = false
            }
        }
    }
    
    override func update(_ currentTime: TimeInterval){
        if (movingLeft || movingRight){
            if (movingLeft){
                player.physicsBody!.velocity = CGVector(dx: -200.0, dy: player.physicsBody!.velocity.dy)
                if(facing)
                {
                    facing = false
                }
                player.xScale = abs(player.xScale) * -1.0
                if(!isAnimated && !jumping){
                    playerAnimate()
                    isAnimated = true
                }
            }
            else{
                player.physicsBody!.velocity = CGVector(dx: 200.0, dy: player.physicsBody!.velocity.dy)
                if(!facing){
                    facing = true
                }
                player.xScale = abs(player.xScale) * 1.0
                if(!isAnimated && !jumping){
                    playerAnimate()
                    isAnimated = true
                }
            }
        }
        else {
            player.physicsBody!.velocity = CGVector(dx: 0.0, dy: player.physicsBody!.velocity.dy)
            player.removeAction(forKey: "playerIsWalking")
            isAnimated = false
        }
        camera!.position = CGPoint(x: player.position.x , y: player.position.y)
        
        
    }
    
    //move player in a direction
    func movePlayer(moveBy: CGFloat, forTheKey: String) {
        player.run(SKAction.moveBy(x: moveBy, y:0, duration: 1))
        
    }
}
extension TutorialScene: SKPhysicsContactDelegate {
    
}
