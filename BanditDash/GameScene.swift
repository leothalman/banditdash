import SpriteKit

class GameScene: SKScene {
    
    struct PhysicsCategory {
        static let none      : UInt32 = 0
        static let all       : UInt32 = UInt32.max
        static let floor     : UInt32 = 0b1       // 1
        static let hazard    : UInt32 = 0b10      // 2
        static let pBit      : UInt32 = 0b100
        static let enemy     : UInt32 = 0b1000
        static let endFlag   : UInt32 = 0b10000
        static let platform  : UInt32 = 0b100000
    }
    
    var arraySprites :[SKSpriteNode] = [SKSpriteNode]()
    var moveNum: Int = 0
    var enemyMoveDistance: Int = 100
    var movingLeft: Bool = false
    var movingRight: Bool = false
    var jumping: Bool = false
    var dashing: Bool = false
    //true if facing right, false if facing left
    var facing: Bool = true
    var canDash: Bool = true
    var cam: SKCameraNode?
    var isAnimated: Bool = false
    
    var tile: SKTileGroup?
    var spike: SKTileGroup?
    var leftspike: SKTileGroup?
    var rightspike: SKTileGroup?
    var downspike: SKTileGroup?
    
    var levelBackground:SKTileMapNode!
    var hazards:SKTileMapNode!
    
    let player = SKSpriteNode(imageNamed: "player-idle")
    var playerWalking: [SKTexture] = []
    let endFlag = SKSpriteNode (imageNamed: "goal")
    
    
    let lButton = SKSpriteNode(imageNamed: "left")
    let rButton = SKSpriteNode(imageNamed: "right")
    let dButton = SKSpriteNode(imageNamed: "dash")
    let jButton = SKSpriteNode(imageNamed: "jump")

    var scoreNum: Int = 0
    let score = SKLabelNode(fontNamed: "Chalkduster")
    
    override func didMove(to view: SKView) {
        //create the level
        createLevel()
        
        physicsWorld.contactDelegate = self
        
        //player
        player.position = CGPoint(x: 0, y: -256)
        player.size = CGSize(width: 32, height: 32)
        player.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: 31,
                                                               height: 31))
        player.physicsBody?.affectedByGravity = true
        player.physicsBody?.allowsRotation = false
        player.physicsBody?.restitution = 0.0
        player.physicsBody?.categoryBitMask = PhysicsCategory.pBit
        player.physicsBody?.collisionBitMask = PhysicsCategory.floor + PhysicsCategory.platform
        player.physicsBody?.usesPreciseCollisionDetection = true
        
        buildPlayerAnimation()
        
        addChild(player)
        
        
        //camera
        cam = SKCameraNode()
        self.camera = cam
        camera!.position.y = 100
        self.addChild(cam!)
        
        //end test
        score.text = "Score: " + scoreNum.description
        score.fontSize = 25
        score.fontColor = SKColor.black
        score.position = CGPoint(x: 0, y: 140)
        self.camera?.addChild(score)
        
        
        //left button properties
        lButton.position = CGPoint(x: -250 , y: -140 )
        lButton.name = "leftButton"
        lButton.size = CGSize(width: lButton.size.width, height: lButton.size.height)
        self.camera?.addChild(lButton)
        
        //right button properties
        rButton.position = CGPoint(x: -120 , y: -140 )
        rButton.name = "rightButton"
        rButton.size = CGSize(width: rButton.size.width, height: rButton.size.height)
        self.camera?.addChild(rButton)
        
        //dash button properties
        dButton.position = CGPoint(x:  120 , y: -140 )
        dButton.name = "dash"
        dButton.size = CGSize(width: dButton.size.width, height: dButton.size.height)
        self.camera?.addChild(dButton)
        
        //jump button properties
        jButton.position = CGPoint(x: 250 , y: -140 )
        jButton.name = "jump"
        jButton.size = CGSize(width: jButton.size.width, height: jButton.size.height)
        self.camera?.addChild(jButton)
    }
    
    func buildPlayerAnimation() {
        let playerAnimatedAtlas = SKTextureAtlas(named: "playerMoving")
        var walkFrames: [SKTexture] = []
        
        let numImages = playerAnimatedAtlas.textureNames.count
        for i in 2...numImages {
            let playerTextureName = "player-move\(i)"
            walkFrames.append(playerAnimatedAtlas.textureNamed(playerTextureName))
        }
        playerWalking = walkFrames
    }
    
    func playerAnimate(){
        player.run(SKAction.repeatForever(
            SKAction.animate(with: playerWalking, timePerFrame: 0.1,
                             resize: false, restore: true)),
            withKey: "playerIsWalking")
    }
    
    func didBegin(_ contact: SKPhysicsContact) {
        var firstBody: SKPhysicsBody
        var secondBody: SKPhysicsBody
        if contact.bodyA.categoryBitMask < contact.bodyB.categoryBitMask {
            firstBody = contact.bodyA
            secondBody = contact.bodyB
        }
        else {
            firstBody = contact.bodyB
            secondBody = contact.bodyA
        }
        
        if (firstBody.categoryBitMask == PhysicsCategory.floor) &&
            (secondBody.categoryBitMask == PhysicsCategory.pBit){
            let test = CGFloat(1)
            let test2 = CGFloat(-1)
            if(jumping && (player.physicsBody?.velocity.dy)! < test && (player.physicsBody?.velocity.dy)! > test2)
            {
                jumping = false
                self.player.texture = SKTexture(imageNamed: "player-idle")
                if (movingRight || movingLeft){
                    playerAnimate()
                    if(!isAnimated){
                        isAnimated = true
                    }
                }
            }
            
        }
        if (firstBody.categoryBitMask == PhysicsCategory.pBit) &&
            (secondBody.categoryBitMask == PhysicsCategory.platform){
            if(jumping)
            {
                jumping = false
                self.player.texture = SKTexture(imageNamed: "player-idle")
                if (movingRight || movingLeft){
                    playerAnimate()
                    if(!isAnimated){
                        isAnimated = true
                    }
                }
            }
        }
        
        if (firstBody.categoryBitMask == PhysicsCategory.hazard) &&
            (secondBody.categoryBitMask == PhysicsCategory.pBit) {
            let transition:SKTransition = SKTransition.fade(withDuration: 1)
            let scene:SKScene = EndScene(size: self.size)
            self.view?.presentScene(scene, transition: transition)
        }
        if (firstBody.categoryBitMask == PhysicsCategory.pBit) &&
            (secondBody.categoryBitMask == PhysicsCategory.endFlag) {
            let transition:SKTransition = SKTransition.fade(withDuration: 1)
            let scene:GameScene = GameScene(size: self.size)
            scene.scoreNum = scoreNum
            self.view?.presentScene(scene, transition: transition)
        }
        if (firstBody.categoryBitMask == PhysicsCategory.pBit) &&
            (secondBody.categoryBitMask == PhysicsCategory.enemy) {
            if (dashing){
                secondBody.node?.removeFromParent()
                scoreNum = scoreNum + 100
                score.text = "Score: " + scoreNum.description
            }
            else{
                let transition:SKTransition = SKTransition.fade(withDuration: 1)
                let scene:SKScene = EndScene(size: self.size)
                self.view?.presentScene(scene, transition: transition)
            }
        }
        
    }
    
    func createLevel()
    {
        let r = CGFloat(0.97)
        let g = CGFloat(0.80)
        let b = CGFloat(0.68)
        let a = CGFloat(1.0)
        self.backgroundColor = UIColor(red: r, green: g, blue: b, alpha: a)
        
        //distance between objects
        var objDistance = 10
        
        var objNumber = 0
        //Tile columns
        let col = 300
        //Tile rows
        let row = 20
        //tile sizes
        let xSize = 32
        let ySize = 32

        guard let tileSet = SKTileSet(named: "BackgroundTiles") else
        {
            fatalError("Background Tiles not found")
        }
        let levelBackground = SKTileMapNode(tileSet: tileSet, columns: col, rows: row, tileSize: CGSize(width: xSize, height: ySize))
        let hazards = SKTileMapNode(tileSet: tileSet, columns: col, rows: row, tileSize: CGSize(width: xSize, height: ySize))
        levelBackground.position = CGPoint(x: ((col-10)/2) * xSize, y: 0)
        levelBackground.setScale(1)
        hazards.position = CGPoint(x: ((col-10)/2) * xSize, y: 0)
        hazards.setScale(1)
        self.addChild(hazards)
        self.addChild(levelBackground)
        self.hazards = hazards
        self.levelBackground = levelBackground
        
        guard let tile = levelBackground.tileSet.tileGroups.first(where: {$0.name == "floor"})
            else {fatalError("Floor Tile not found")}
        guard let spike = hazards.tileSet.tileGroups.first(where: {$0.name == "spikes"})
            else {fatalError("Spikes Tile not foud")}
        guard let leftspike = hazards.tileSet.tileGroups.first(where: {$0.name == "spikesLeft"})
            else {fatalError("Spikes Tile not foud")}
        guard let rightspike = hazards.tileSet.tileGroups.first(where: {$0.name == "spikesRight"})
            else {fatalError("Spikes Tile not foud")}
        guard let downspike = hazards.tileSet.tileGroups.first(where: {$0.name == "spikesDown"})
            else {fatalError("Spikes Tile not foud")}
        self.spike = spike
        self.tile = tile
        self.leftspike = leftspike
        self.rightspike = rightspike
        self.downspike = downspike
        for r in 0...(row-1) {
            levelBackground.setTileGroup(tile, forColumn:0, row: r)
            levelBackground.setTileGroup(tile, forColumn:col-1, row: r)
        }
        var test = 0
        var enemy = SKSpriteNode (imageNamed: "enemy1")
        objNumber = Int(arc4random_uniform(8)) + 1
        
        for column in 0...(col-1) {
            levelBackground.setTileGroup(tile, forColumn: column, row: 0)
            //set an object every 10 tiles
            if (test == objDistance && column < (col-11))
            {
                test = 0
                objNumber = Int(arc4random_uniform(8)) + 1
        
                if (objNumber == 1)
                {
                    objDistance = 8
                    hazards.setTileGroup(spike, forColumn:column, row: 1)
                    hazards.setTileGroup(spike, forColumn:column+1, row: 1)
                }
                if (objNumber == 2)
                {
                    objDistance = 8
                    levelBackground.setTileGroup(tile, forColumn: column, row: 1)
                    levelBackground.setTileGroup(tile, forColumn: column, row: 2)
                    
                    
                }
                
                if (objNumber == 3)
                {
                    objDistance = 8
                    levelBackground.setTileGroup(tile, forColumn: column, row: 2)
                    levelBackground.setTileGroup(tile, forColumn: column+1, row: 2)
                    levelBackground.setTileGroup(tile, forColumn: column+2, row: 2)
                }
                
                if (objNumber == 4)
                {
                    objDistance = 8
                    enemy = SKSpriteNode (imageNamed: "enemy1")
                    enemy.position = CGPoint(x: (column - 4) * 32 , y: -256)
                    enemy.size = CGSize(width: 32, height: 32)
                    enemy.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: 32,
                                                                          height: 32))
                    enemy.physicsBody?.affectedByGravity = true
                    enemy.physicsBody?.allowsRotation = false
                    enemy.physicsBody?.restitution = 0.0
                    enemy.physicsBody?.categoryBitMask = PhysicsCategory.enemy
                    enemy.physicsBody?.collisionBitMask = PhysicsCategory.floor + PhysicsCategory.hazard
                    enemy.physicsBody?.usesPreciseCollisionDetection = true
                    enemy.physicsBody?.contactTestBitMask = PhysicsCategory.pBit
                    
                    let wait = SKAction.wait(forDuration:1.5)

                    let move = SKAction.moveBy(x:100, y:0.01, duration: 0.5)
                    let move2 = SKAction.moveBy(x:-100, y:0.01, duration: 0.5)
                    var arr = [SKAction]()
                    var randInt = 0
                    for temp in 0...5{
                        randInt = Int(arc4random_uniform(10))
                        if(randInt < 5){
                        arr.append(move)
                        }
                        else{
                        arr.append(move2)
                        }
                    }
                    var moveSeq = SKAction.sequence([arr[0], wait, arr[1], wait, arr[2], wait, arr[3], wait, arr[4], wait])
                    
                    
                    var enemyMoveSeq = SKAction.repeatForever(moveSeq)
                    
                    enemy.run(enemyMoveSeq)
                    arraySprites.append(enemy)
                    self.addChild(enemy)
                }
                if (objNumber == 5){
                    addSpikeWall(column: column)
                    objDistance = 15
                }
                if (objNumber == 6){
                    addSnakeTunnel(column: column)
                    objDistance = 15
                }
                if(objNumber == 7){
                    addSpikeTunnel(column: column)
                    objDistance = 20
                }
                if(objNumber == 8){
                    addSpikeTower(column: column)
                    objDistance = 20
                }
                
            }
            else{
                test += 1
            }
            
        }
        endFlag.size = CGSize(width: 32, height: 32)
        endFlag.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: endFlag.size.width,
                                                         height: endFlag.size.height))
        endFlag.position = CGPoint(x: (col-8) * 32 , y: -256)
        endFlag.physicsBody?.categoryBitMask = PhysicsCategory.endFlag
        endFlag.physicsBody?.contactTestBitMask = PhysicsCategory.pBit
        endFlag.physicsBody?.affectedByGravity = true
        endFlag.physicsBody?.allowsRotation = false
        endFlag.physicsBody?.usesPreciseCollisionDetection = true
        self.addChild(endFlag)
        
        giveFloorPhysicsBody(map: levelBackground)
        giveSpikePhysicsBody(map: hazards)
    }
    
    func addSpikeWall(column: Int)
    {
        var platform1 = SKSpriteNode(imageNamed: "floor")
        var platform2 = SKSpriteNode(imageNamed: "floor")
        platform1.size = CGSize(width: 32, height: 32)
        platform2.size = CGSize(width: 64, height: 32)
        platform1.position = CGPoint(x: (column-3) * 32, y: -224 )
        platform2.position = CGPoint(x: (column + 2) * 32, y: -224 )
        
        platform1.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: 32, height: 32))
        platform1.physicsBody?.affectedByGravity = false
        platform1.physicsBody?.allowsRotation = false
        platform1.physicsBody?.isDynamic = false
        platform1.physicsBody?.friction = 1
        
        platform1.physicsBody?.categoryBitMask = PhysicsCategory.platform
        platform1.physicsBody?.collisionBitMask = PhysicsCategory.pBit
        platform1.physicsBody?.contactTestBitMask = PhysicsCategory.pBit
        
        platform2.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: 64, height: 32))
        platform2.physicsBody?.affectedByGravity = false
        platform2.physicsBody?.allowsRotation = false
        platform2.physicsBody?.isDynamic = false
        platform2.physicsBody?.friction = 0
        
        platform2.physicsBody?.categoryBitMask = PhysicsCategory.platform
        platform2.physicsBody?.collisionBitMask = PhysicsCategory.pBit
        platform2.physicsBody?.contactTestBitMask = PhysicsCategory.pBit
        
        self.addChild(platform1)
        self.addChild(platform2)
        
        let wait = SKAction.wait(forDuration:1.5)
        
        let moveup = SKAction.moveBy(x:0, y:210, duration: 0.5)
        let movedown = SKAction.moveBy(x:0, y:-210, duration: 0.5)
        
        let platform2move = SKAction.sequence([wait, moveup, wait, movedown])
        platform2.run(SKAction.repeatForever(platform2move))
        
        levelBackground.setTileGroup(tile, forColumn: column+11, row: 1)
        for i in 0...10
        {
            hazards.setTileGroup(spike, forColumn:column+i, row: 1)
            hazards.setTileGroup(leftspike, forColumn:column+10, row: 2+i)
            levelBackground.setTileGroup(tile, forColumn: column+11, row: 2+i)
        }
    }
    
    func addSnakeTunnel(column: Int)
    {
        let snake = SKSpriteNode (imageNamed: "enemy1")
        snake.position = CGPoint(x: (column - 4) * 32 , y: -160)
        snake.size = CGSize(width: 32, height: 32)
        snake.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: 32,
                                                              height: 32))
        snake.physicsBody?.affectedByGravity = true
        snake.physicsBody?.allowsRotation = false
        snake.physicsBody?.restitution = 0.0
        snake.physicsBody?.categoryBitMask = PhysicsCategory.enemy
        snake.physicsBody?.collisionBitMask = PhysicsCategory.floor + PhysicsCategory.hazard
        snake.physicsBody?.usesPreciseCollisionDetection = true
        snake.physicsBody?.contactTestBitMask = PhysicsCategory.pBit
        self.addChild(snake)
        
        let wait = SKAction.wait(forDuration:1.5)
        
        let moveright = SKAction.moveBy(x:(32*9), y:0.01, duration: 3)
        let moveleft = SKAction.moveBy(x:-(32*9), y:0.01, duration: 3)
        
        
        let snakemove = SKAction.sequence([wait, moveright, wait, moveleft])
        snake.run(SKAction.repeatForever(snakemove))
        
        levelBackground.setTileGroup(tile, forColumn: column, row: 1)
        levelBackground.setTileGroup(tile, forColumn: column, row: 2)
        levelBackground.setTileGroup(tile, forColumn: column+10, row: 1)
        levelBackground.setTileGroup(tile, forColumn: column+10, row: 2)
        for i in 0...10
        {
            levelBackground.setTileGroup(tile, forColumn:column+i, row: 7)
            hazards.setTileGroup(downspike, forColumn:column+i, row: 6)
            hazards.setTileGroup(spike, forColumn:column+i, row: 8)
            levelBackground.setTileGroup(tile, forColumn: column+i, row: 3)
        }
    }
    
    func addSpikeTunnel(column: Int)
    {
        let platform1 = SKSpriteNode(imageNamed: "floor")
        platform1.size = CGSize(width: 64, height: 32)
        platform1.position = CGPoint(x: ((column-3) * 32), y: -176 )
        platform1.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: 64, height: 32))
        platform1.physicsBody?.affectedByGravity = false
        platform1.physicsBody?.allowsRotation = false
        platform1.physicsBody?.isDynamic = false
        
        platform1.physicsBody?.categoryBitMask = PhysicsCategory.platform
        platform1.physicsBody?.collisionBitMask = PhysicsCategory.pBit
        platform1.physicsBody?.contactTestBitMask = PhysicsCategory.pBit
        self.addChild(platform1)
        
        let pSpike = SKSpriteNode(imageNamed: "leftSpike")
        pSpike.size = CGSize(width: 32, height: 32)
        pSpike.position = CGPoint(x: ((column-4) * 32)-16, y: -176 )
        pSpike.physicsBody = SKPhysicsBody(rectangleOf: CGSize(width: 30, height: 20))
        pSpike.physicsBody?.affectedByGravity = false
        pSpike.physicsBody?.allowsRotation = false
        pSpike.physicsBody?.isDynamic = false
        
        pSpike.physicsBody?.categoryBitMask = PhysicsCategory.hazard
        pSpike.physicsBody?.collisionBitMask = PhysicsCategory.pBit
        pSpike.physicsBody?.contactTestBitMask = PhysicsCategory.pBit
        
        let wait = SKAction.wait(forDuration:1.5)
        
        let moveright = SKAction.moveBy(x:(32*12), y:0, duration: 3)
        let moveleft = SKAction.moveBy(x:-(32*12), y:0, duration: 0.5)
        let pmove = SKAction.sequence([wait, moveright, wait, moveleft])
        platform1.run(SKAction.repeatForever(pmove))
        pSpike.run(SKAction.repeatForever(pmove))
        self.addChild(pSpike)

        levelBackground.setTileGroup(tile, forColumn: column-1, row: 3)
        levelBackground.setTileGroup(tile, forColumn: column-1, row: 4)
        levelBackground.setTileGroup(tile, forColumn: column-1, row: 5)
        
        for i in 0...6{
            levelBackground.setTileGroup(tile, forColumn: column, row: 5+i)
            levelBackground.setTileGroup(tile, forColumn: column+15, row: 1+i)
        }
        for i in 0...14
        {
            if(i>2){
            levelBackground.setTileGroup(tile, forColumn:column+i, row: 7)
            hazards.setTileGroup(downspike, forColumn:column+i, row: 6)

            }
            if(i < 10){
            levelBackground.setTileGroup(tile, forColumn: column+i, row: 3)
            }
        }
    }
    
    func addSpikeTower(column: Int)
    {

        for i in 0...11
        {
            levelBackground.setTileGroup(tile, forColumn:column, row: 2+i)
            levelBackground.setTileGroup(tile, forColumn:column+10, row: 1+i)
        }

        for i in 0...7{
            levelBackground.setTileGroup(tile, forColumn:column+3+i, row: 13)
        }
        levelBackground.setTileGroup(tile, forColumn:column+1, row: 10)
        levelBackground.setTileGroup(tile, forColumn:column+2, row: 10)
        levelBackground.setTileGroup(tile, forColumn:column+3, row: 10)
        var lSide = 0
        var rSide = 0
        for i in 0...1{
            lSide = (i*4) + 2
            rSide = (i*4) + 4
            levelBackground.setTileGroup(tile, forColumn:column+1, row: lSide)
            levelBackground.setTileGroup(tile, forColumn:column+2, row: lSide)
            levelBackground.setTileGroup(tile, forColumn:column+3, row: lSide)
            hazards.setTileGroup(rightspike, forColumn:column+1, row: lSide+1)
            hazards.setTileGroup(rightspike, forColumn:column+1, row: lSide+2)
            hazards.setTileGroup(rightspike, forColumn:column+1, row: lSide+3)
            hazards.setTileGroup(leftspike, forColumn:column+9, row: rSide+1)
            hazards.setTileGroup(leftspike, forColumn:column+9, row: rSide+2)
            hazards.setTileGroup(leftspike, forColumn:column+9, row: rSide+3)
            levelBackground.setTileGroup(tile, forColumn:column+7, row: rSide)
            levelBackground.setTileGroup(tile, forColumn:column+8, row: rSide)
            levelBackground.setTileGroup(tile, forColumn:column+9, row: rSide)
        }
    }
    
    func giveFloorPhysicsBody(map: SKTileMapNode)
    {
        let tileMap = map
        
        let tileSize = tileMap.tileSize
        let halfWidth = CGFloat(tileMap.numberOfColumns) / 2.0 * tileSize.width
        let halfHeight = CGFloat(tileMap.numberOfRows) / 2.0 * tileSize.height
        
        for col in 0..<tileMap.numberOfColumns {
            
            for row in 0..<tileMap.numberOfRows {
                
                if let tileDefinition = tileMap.tileDefinition(atColumn: col, row: row)
                    
                {
                    
                    //let isEdgeTile = tileDefinition.userData?["AddBody"] as? Int  //uncomment this if needed, see article notes
                    //if (isEdgeTile != 0) {
                    let tileArray = tileDefinition.textures
                    let tileTexture = tileArray[0]
                    let x = CGFloat(col) * tileSize.width - halfWidth + (tileSize.width/2)
                    let y = CGFloat(row) * tileSize.height - halfHeight + (tileSize.height/2)
                    _ = CGRect(x: 0, y: 0, width: tileSize.width, height: tileSize.height)
                    let tileNode = SKNode()
                    
                    tileNode.position = CGPoint(x: x, y: y)
                    tileNode.physicsBody = SKPhysicsBody(texture: tileTexture, size: CGSize(width: 32, height: 32))
                    tileNode.physicsBody?.linearDamping = 0.0
                    tileNode.physicsBody?.affectedByGravity = false
                    tileNode.physicsBody?.allowsRotation = false
                    tileNode.physicsBody?.restitution = 0.0
                    tileNode.physicsBody?.isDynamic = false
                    tileNode.physicsBody?.friction = 0
                    
                    tileNode.physicsBody?.categoryBitMask = PhysicsCategory.floor
                    tileNode.physicsBody?.collisionBitMask = PhysicsCategory.pBit
                    tileNode.physicsBody?.contactTestBitMask = PhysicsCategory.pBit
                    
                    
                    //tileNode.physicsBody?.categoryBitMask = BodyType.item.rawValue
                    
                    tileMap.addChild(tileNode)
                    //}
                }
            }
        }
    }
    
    func giveSpikePhysicsBody(map: SKTileMapNode)
    {
        let tileMap = map
        
        let tileSize = tileMap.tileSize
        let halfWidth = CGFloat(tileMap.numberOfColumns) / 2.0 * tileSize.width
        let halfHeight = CGFloat(tileMap.numberOfRows) / 2.0 * tileSize.height
        
        for col in 0..<tileMap.numberOfColumns {
            
            for row in 0..<tileMap.numberOfRows {
                
                if let tileDefinition = tileMap.tileDefinition(atColumn: col, row: row)
                    
                {
                    
                    //let isEdgeTile = tileDefinition.userData?["AddBody"] as? Int  //uncomment this if needed, see article notes
                    //if (isEdgeTile != 0) {
                    let tileArray = tileDefinition.textures
                    let tileTexture = tileArray[0]
                    let x = CGFloat(col) * tileSize.width - halfWidth + (tileSize.width/2)
                    let y = CGFloat(row) * tileSize.height - halfHeight + (tileSize.height/2)
                    _ = CGRect(x: 0, y: 0, width: tileSize.width, height: tileSize.height)
                    let tileNode = SKNode()
                    
                    tileNode.position = CGPoint(x: x, y: y)
                    tileNode.physicsBody = SKPhysicsBody(texture: tileTexture, size: CGSize(width: 30 , height: 30))
                    tileNode.physicsBody?.linearDamping = 0.0
                    tileNode.physicsBody?.affectedByGravity = false
                    tileNode.physicsBody?.allowsRotation = false
                    tileNode.physicsBody?.restitution = 0.0
                    tileNode.physicsBody?.isDynamic = false
                    
                    tileNode.physicsBody?.categoryBitMask = PhysicsCategory.hazard
                    tileNode.physicsBody?.collisionBitMask = PhysicsCategory.none
                    tileNode.physicsBody?.contactTestBitMask = PhysicsCategory.pBit
                    
                    
                    //tileNode.physicsBody?.categoryBitMask = BodyType.item.rawValue
                    
                    tileMap.addChild(tileNode)
                    //}
                }
            }
        }
    }
    
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch: AnyObject in touches {
            let location = touch.location(in:self)
            let node = self.atPoint(location)
            
            if (node.name == "leftButton") {
                movingLeft = true
            }
            if (node.name == "rightButton"){
                movingRight = true
            }
            if (node.name == "dash" && canDash) {
                var dashDistance = CGFloat(-100.0)
                if(facing){
                    dashDistance = CGFloat(100.0)
                }
                self.dashing = true
                self.canDash = false
                dButton.texture = SKTexture(imageNamed: "cooldown")
                let wait = SKAction.wait(forDuration:0.8)
                let unflagCanDash = SKAction.run {
                    self.canDash = true
                    self.dButton.texture = SKTexture(imageNamed: "dash")
                }
                let animateDash = SKAction.run{
                    if(self.movingLeft || self.movingRight){
                        self.player.removeAction(forKey: "playerIsWalking")
                    }
                    self.player.texture = SKTexture(imageNamed: "player-move1")
                }
                let dash = SKAction.moveBy(x:dashDistance, y:0.01, duration: 0.1)
                let unflagDashing = SKAction.run {
                    self.dashing = false
                    if(!self.jumping){
                        if(self.movingLeft || self.movingRight){
                            self.playerAnimate()
                        }
                        self.player.texture = SKTexture(imageNamed: "player-idle")
                    }
                }
                let dashSeq = SKAction.sequence([animateDash, dash, unflagDashing, wait, unflagCanDash])
                player.run(dashSeq)
                }
            
            if (node.name == "jump")
            {
                if(!jumping){
                    jumping = true
                    if (movingRight || movingLeft){
                        self.player.removeAction(forKey: "playerIsWalking")
                    }
                    self.player.texture = SKTexture(imageNamed: "player-jump")
                    
                    self.player.physicsBody!.applyImpulse(CGVector(dx:0 , dy: 25))
                }
                
            }
        
        }
    }
    
    //check which button is pressed and move the player in that direction
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?){
        for touch: AnyObject in touches {
            let location = touch.location(in:self)
            let node = self.atPoint(location)
            
            if (node.name == "leftButton") {
                movingLeft = true
            }
            else {
                movingLeft = false
            }
            if (node.name == "rightButton"){
                movingRight = true
            }
            else {
                movingRight = false
            }
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        for touch: AnyObject in touches {
            let location = touch.location(in:self)
            let node = self.atPoint(location)
            
            if (node.name == "leftButton") {
                movingLeft = false
            }
            if (node.name == "rightButton"){
                movingRight = false
            }
        }
    }
    
    override func update(_ currentTime: TimeInterval){
        if (movingLeft || movingRight){
            if (movingLeft){
                player.physicsBody!.velocity = CGVector(dx: -200.0, dy: player.physicsBody!.velocity.dy)
                if(facing)
                {
                    facing = false
                }
                player.xScale = abs(player.xScale) * -1.0
                if(!isAnimated && !jumping){
                    playerAnimate()
                    isAnimated = true
                }
            }
            else{
                player.physicsBody!.velocity = CGVector(dx: 200.0, dy: player.physicsBody!.velocity.dy)
                if(!facing){
                    facing = true
                }
                player.xScale = abs(player.xScale) * 1.0
                if(!isAnimated && !jumping){
                    playerAnimate()
                    isAnimated = true
                }
            }
        }
        else {
            player.physicsBody!.velocity = CGVector(dx: 0.0, dy: player.physicsBody!.velocity.dy)
            player.removeAction(forKey: "playerIsWalking")
            isAnimated = false
        }
        camera!.position = CGPoint(x: player.position.x , y: player.position.y)
        

    }
    
    //move player in a direction
    func movePlayer(moveBy: CGFloat, forTheKey: String) {
        player.run(SKAction.moveBy(x: moveBy, y:0, duration: 1))

    }
}
extension GameScene: SKPhysicsContactDelegate {
    
}
